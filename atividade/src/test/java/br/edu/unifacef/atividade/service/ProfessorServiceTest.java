package br.edu.unifacef.atividade.service;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.atividade.dao.ProfessorDAO;
import br.unifacef.atividade.dto.Professor;
import br.unifacef.atividade.service.ProfessorService;

@RunWith(MockitoJUnitRunner.class)
public class ProfessorServiceTest {

	@Mock
	private ProfessorDAO dao;
	
	@Test
	public void deveSalvarUmNovoProfessor() {		
		Professor professorEsperadoMock = new Professor();
		professorEsperadoMock.setId(1);
		professorEsperadoMock.setNome("Iago Colbacho Bettarello");
		professorEsperadoMock.setEmail("bettarello.iago@gmail.com");
		professorEsperadoMock.setDataNasc(new Date());
		professorEsperadoMock.setCep("14620000");
		professorEsperadoMock.setLogradouro("R 1");
		professorEsperadoMock.setNumero("1234");
		professorEsperadoMock.setBairro("Centro");
		professorEsperadoMock.setCidade("Orlândia");
		professorEsperadoMock.setEstado("SP");
		
		Professor novoProfessor = new Professor();
		novoProfessor.setId(1234);
		novoProfessor.setNome("Tatiane Patrícia Lima");
		novoProfessor.setEmail("tatianepatricialima@consultorialk.com.br");
		novoProfessor.setDataNasc(new Date());
		novoProfessor.setCep("97030842");
		novoProfessor.setLogradouro("Rua Vereador José Manoel da Silveira");
		novoProfessor.setNumero("192");
		novoProfessor.setBairro("Pinheiro Machado");
		novoProfessor.setCidade("Santa Maria");
		novoProfessor.setEstado("RS");
		
		Mockito.when(dao.salvarProfessor(novoProfessor)).thenReturn(professorEsperadoMock);
		
		ProfessorService professorService = new ProfessorService(dao);
		Professor professorRegistrado = professorService.salvarProfessor(novoProfessor);
		
		System.out.println(professorRegistrado);
		
		assertNotNull(professorRegistrado);
		assertNotNull(professorRegistrado.getId());		
	}
	
	@Test
	public void deveRetornarProfessorPorId() {
		Integer id = 1;
		
		Professor professorEsperadoMock = new Professor();
		professorEsperadoMock.setId(1);
		professorEsperadoMock.setNome("Iago Colbacho Bettarello");
		professorEsperadoMock.setEmail("bettarello.iago@gmail.com");
		professorEsperadoMock.setDataNasc(new Date());
		professorEsperadoMock.setCep("14620000");
		professorEsperadoMock.setLogradouro("R 1");
		professorEsperadoMock.setNumero("1234");
		professorEsperadoMock.setBairro("Centro");
		professorEsperadoMock.setCidade("Orlândia");
		professorEsperadoMock.setEstado("SP");
		
		ProfessorService professorService = new ProfessorService(dao);
		
		Mockito.when(dao.buscarProfessorPorId(id)).thenReturn(professorEsperadoMock);		
		
		Professor professorEncontrado = professorService.buscarProfessorPorId(id);
		
		System.out.println(professorEncontrado);
		
		assertNotNull(professorEncontrado);
		assertNotNull(professorEncontrado.getId());
	}
	
	@Test
	public void deveRetornarProfessorPorNome() {
		String nome = "Fulano";
		
		Professor p1Mock = new Professor();
		p1Mock.setId(new Double(Math.random() * 100).intValue());
		p1Mock.setNome("Iago Colbacho Bettarello");
		p1Mock.setEmail("bettarello.iago@gmail.com");
		p1Mock.setDataNasc(new Date());
		p1Mock.setCep("14620000");
		p1Mock.setLogradouro("R 1");
		p1Mock.setNumero("1234");
		p1Mock.setBairro("Centro");
		p1Mock.setCidade("Orlândia");
		p1Mock.setEstado("SP");
		
		Professor p2Mock = new Professor();
		p2Mock.setId(new Double(Math.random() * 100).intValue());
		p2Mock.setNome("Tatiane Patrícia Lima");
		p2Mock.setEmail("tatianepatricialima@consultorialk.com.br");
		p2Mock.setDataNasc(new Date());
		p2Mock.setCep("97030842");
		p2Mock.setLogradouro("Rua Vereador José Manoel da Silveira");
		p2Mock.setNumero("192");
		p2Mock.setBairro("Pinheiro Machado");
		p2Mock.setCidade("Santa Maria");
		p2Mock.setEstado("RS");
		
		List<Professor> professoresMock = Arrays.asList(p1Mock, p2Mock);
		
		ProfessorService professorService = new ProfessorService(dao);
		
		Mockito.when(professorService.buscarProfessorPorNome(nome)).thenReturn(professoresMock);
		
		List<Professor> professoresEncontrados = professorService.buscarProfessorPorNome(nome);
		
		System.out.println(professoresEncontrados);
		
		assertNotNull(professoresEncontrados);
		
	}
}
