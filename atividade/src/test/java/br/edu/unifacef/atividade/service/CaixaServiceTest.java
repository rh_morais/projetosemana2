package br.edu.unifacef.atividade.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.math.BigDecimal;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.atividade.dao.CaixaDAO;
import br.unifacef.atividade.dto.Caixa;
import br.unifacef.atividade.service.CaixaService;

@RunWith(MockitoJUnitRunner.class)

public class CaixaServiceTest {
	
	@Mock
	private CaixaDAO caixaDAO;
	
	@Test
	public void deveriaSalvarUmNovoCaixa() {
		
		Caixa caixaEsperadoMock = new Caixa();
		caixaEsperadoMock.setId(123);
		caixaEsperadoMock.setTaxa(new BigDecimal(1999));
		caixaEsperadoMock.setTotal(new BigDecimal(3000));
		caixaEsperadoMock.setDesconto(new BigDecimal(300));
		caixaEsperadoMock.setTotalPagar();
		
		Caixa novoCaixa = new Caixa();
		
		novoCaixa.setTaxa(new BigDecimal(1999));
		novoCaixa.setTotal(new BigDecimal(3000));
		novoCaixa.setDesconto(new BigDecimal(300));
		novoCaixa.setTotalPagar();
		
		Mockito.when(caixaDAO.realizarVenda(novoCaixa)).thenReturn(caixaEsperadoMock);
		
		CaixaService caixaService = new CaixaService(caixaDAO);
		
		Caixa caixaRegistrado = caixaService.realizarVenda(novoCaixa);
		
		System.out.println(caixaRegistrado);
		assertNotNull(caixaRegistrado);
		assertNotNull(caixaRegistrado.getId());
		
	}
	
	@Test
	public void deveRetornarOCalculoCorreto() {
		
		Caixa caixaEsperadoMock = new Caixa();
		caixaEsperadoMock.setId(123);
		caixaEsperadoMock.setTaxa(new BigDecimal(1999));
		caixaEsperadoMock.setTotal(new BigDecimal(3000));
		caixaEsperadoMock.setDesconto(new BigDecimal(300));
		caixaEsperadoMock.setTotalPagar();
		
		Caixa novoCaixa = new Caixa();
		
		novoCaixa.setTaxa(new BigDecimal(1999));
		novoCaixa.setTotal(new BigDecimal(3000));
		novoCaixa.setDesconto(new BigDecimal(300));
		novoCaixa.setTotalPagar();
		
		Mockito.when(caixaDAO.realizarVenda(novoCaixa)).thenReturn(caixaEsperadoMock);
		
		CaixaService caixaService = new CaixaService(caixaDAO);
		
		Caixa caixaRegistrado = caixaService.realizarVenda(novoCaixa);
		assertEquals(new BigDecimal(4699), caixaRegistrado.totalPagar);
		
	}

	@Test
	public void deveRetornarATaxaCorreta() {
		
		Caixa caixaEsperadoMock = new Caixa();
		caixaEsperadoMock.setId(123);
		caixaEsperadoMock.setTaxa(new BigDecimal(1999));
		caixaEsperadoMock.setTotal(new BigDecimal(3000));
		caixaEsperadoMock.setDesconto(new BigDecimal(300));
		caixaEsperadoMock.setTotalPagar();
		
		Caixa novoCaixa = new Caixa();
		
		novoCaixa.setTaxa(new BigDecimal(1999));
		novoCaixa.setTotal(new BigDecimal(3000));
		novoCaixa.setDesconto(new BigDecimal(300));
		novoCaixa.setTotalPagar();
		
		Mockito.when(caixaDAO.realizarVenda(novoCaixa)).thenReturn(caixaEsperadoMock);
		
		CaixaService caixaService = new CaixaService(caixaDAO);
		
		Caixa caixaRegistrado = caixaService.realizarVenda(novoCaixa);
		assertEquals(new BigDecimal(1999), caixaRegistrado.getTaxa());
		
	}

}
