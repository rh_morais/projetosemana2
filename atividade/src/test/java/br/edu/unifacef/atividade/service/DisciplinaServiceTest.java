package br.edu.unifacef.atividade.service;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.atividade.dao.DisciplinaDAO;
import br.unifacef.atividade.dto.Disciplina;
import br.unifacef.atividade.service.DisciplinaService;

@RunWith(MockitoJUnitRunner.class)
public class DisciplinaServiceTest {
	
	@Mock
	private DisciplinaDAO disciplinaDAO;
	
	@Test
	public void deveSalvarUmaNovaDisciplina() {		
		Disciplina disciplinaEsperadaMock = new Disciplina();
		disciplinaEsperadaMock.setId(123);
		disciplinaEsperadaMock.setNome("Matemática");
		disciplinaEsperadaMock.setPeriodo("Manhã");
		
		Disciplina novaDisciplina = new Disciplina();
		novaDisciplina.setNome("Geografia");
		novaDisciplina.setPeriodo("Noite");
		
		Mockito.when(disciplinaDAO.salvarDisciplina(novaDisciplina)).thenReturn(disciplinaEsperadaMock);
		
		DisciplinaService disciplinaService = new DisciplinaService(disciplinaDAO);
		
		Disciplina disciplinaRegistrada = disciplinaService.salvarDisciplina(novaDisciplina);
		
		System.out.println(disciplinaRegistrada);
		
		assertNotNull(disciplinaRegistrada);
		assertNotNull(disciplinaRegistrada.getId());		
	}
	
	@Test
	public void deveRetornarDisciplinaPorId() {
		Integer id = 1;
		
		Disciplina disciplinaEsperadaMock = new Disciplina();
		disciplinaEsperadaMock.setId(12);
		disciplinaEsperadaMock.setNome("Biologia");
		disciplinaEsperadaMock.setPeriodo("Tarde");
		
		DisciplinaService disciplinaService = new DisciplinaService(disciplinaDAO);
		
		Mockito.when(disciplinaDAO.buscaDisciplinaPorId(id)).thenReturn(disciplinaEsperadaMock);		
		
		Disciplina disciplinaEncontrada = disciplinaService.buscaDisciplinaPorId(id);
		
		System.out.println(disciplinaEncontrada);
		
		assertNotNull(disciplinaEncontrada);
		assertNotNull(disciplinaEncontrada.getId());
	}
	
	@Test
	public void deveRetornarDisciplinasPorPeriodo() {
		String periodo = "Manhã";
		
		Disciplina d1Mock = new Disciplina();
		d1Mock.setId(new Double(Math.random() * 100).intValue());
		d1Mock.setNome("Português");
		d1Mock.setPeriodo(periodo);
		
		Disciplina d2Mock = new Disciplina();
		d2Mock.setId(new Double(Math.random() * 100).intValue());
		d2Mock.setNome("Matemática");
		d2Mock.setPeriodo(periodo);
		
		List<Disciplina> disciplinasMock = Arrays.asList(d1Mock, d2Mock);
		
		DisciplinaService disciplinaService = new DisciplinaService(disciplinaDAO);
		
		Mockito.when(disciplinaService.buscaDisciplinasPorPeriodo(periodo)).thenReturn(disciplinasMock);
		
		List<Disciplina> disciplinasEncontradas = disciplinaService.buscaDisciplinasPorPeriodo(periodo);
		
		System.out.println(disciplinasEncontradas);
		
		assertNotNull(disciplinasEncontradas);
		
	}
}
