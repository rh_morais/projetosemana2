package br.edu.unifacef.atividade.service;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.atividade.dao.AlunoDAO;
import br.unifacef.atividade.dto.Aluno;
import br.unifacef.atividade.service.AlunoService;

@RunWith(MockitoJUnitRunner.class)
public class AlunoServiceTest {
	
	@Mock
	private AlunoDAO alunoDAO;
	
	@Test
	public void deveriaSalvarUmNovoAluno() {
	
		Aluno alunoEsperadoMock = new Aluno(1, "teste", (byte)25);
		
		Aluno novoAluno = new Aluno();
		novoAluno.setIdade((byte)20);
		novoAluno.setNome("teste");
		
		Mockito.when(alunoDAO.salvarAluno(novoAluno)).thenReturn(alunoEsperadoMock);
		
		AlunoService alunoService = new AlunoService(alunoDAO);
		
		Aluno alunoSalvo = alunoService.salvarAluno(novoAluno);
		
		assertNotNull(alunoSalvo);
		assertNotNull(alunoSalvo.getId());
		assertEquals(alunoSalvo.getId(), alunoEsperadoMock.getId());
	}	
	
	@Test
	public void deveriaRetornarAlunoPorNome() {
		
		String nome = "teste";
		
		Aluno aluno1Mock = new Aluno(1, "rafael", (byte)22);
		Aluno aluno2Mock = new Aluno(2, "rafael Morais", (byte)22);
		
		List<Aluno> listaMock = Arrays.asList(aluno1Mock, aluno2Mock);
				
		AlunoService alunoService = new AlunoService(alunoDAO);
		
		Mockito.when(alunoService.buscarAlunoPorNome(nome)).thenReturn(listaMock);
		
		List<Aluno> lista = alunoService.buscarAlunoPorNome(nome);
		
		assertNotNull(lista);				
	}
	
	@Test
	public void deveriaRetornarAlunoPorId() {
		
		int id = 1;
		
		Aluno aluno1Mock = new Aluno(1, "rafael", (byte)22);					
				
		AlunoService alunoService = new AlunoService(alunoDAO);
		
		Mockito.when(alunoService.buscarAlunoPorId(id)).thenReturn(aluno1Mock);
		
		Aluno alunoBusca = alunoService.buscarAlunoPorId(id);
		
		assertNotNull(alunoBusca);
		assertEquals(aluno1Mock.getId(), alunoBusca.getId());
	}
}