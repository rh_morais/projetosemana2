package br.edu.unifacef.atividade.service;

import static org.junit.Assert.assertNotNull;

import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import br.unifacef.atividade.dao.BibliotecaDAO;
import br.unifacef.atividade.dto.Biblioteca;
import br.unifacef.atividade.service.BibliotecaService;

@RunWith(MockitoJUnitRunner.class)
public class BibliotecaServiceTest {
	@Mock
	private BibliotecaDAO bibliotecaDAO;
	
	@Test
	public void deveSalvarUmNovoLivro() {		
		Biblioteca livroMock = new Biblioteca();
		livroMock.setId(1);
		livroMock.setNome("Harry Potter");
		livroMock.setDisponibilidade("Disponivel");
		
		Biblioteca novoLivro = new Biblioteca();
		novoLivro.setNome("Game of Thrones");
		novoLivro.setDisponibilidade("Indisponivel");
		
		Mockito.when(bibliotecaDAO.salvarLivro(novoLivro)).thenReturn(livroMock);
		
		BibliotecaService bibliotecaService = new BibliotecaService(bibliotecaDAO);
		
		Biblioteca livroRegistrado = bibliotecaService.salvarLivro(novoLivro);
		
		System.out.println("deveSalvarUmNovoLivro: " + livroRegistrado);
		
		assertNotNull(livroRegistrado);
		assertNotNull(livroRegistrado.getId());		
	}
	
	@Test
	public void deveRetornarLivroPorId() {
		Integer id = 1;
		
		Biblioteca livroMock = new Biblioteca();
		livroMock.setId(10);
		livroMock.setNome("Harry Potter");
		livroMock.setDisponibilidade("Disponivel");
		
		BibliotecaService bibliotecaService = new BibliotecaService(bibliotecaDAO);
		
		Mockito.when(bibliotecaDAO.buscaLivroPorId(id)).thenReturn(livroMock);		
		
		Biblioteca livroEncontrado = bibliotecaService.buscaLivroPorId(id);
		
		System.out.println("deveRetornarLivroPorId: " + livroEncontrado);
		
		assertNotNull(livroEncontrado);
		assertNotNull(livroEncontrado.getId());
	}
	
	@Test
	public void deveRetornarLivroPorDisponibilidade() {
		String disponibilidade = "Disponivel";
		
		Biblioteca livro1M = new Biblioteca();
		livro1M.setId(new Double(Math.random() * 100).intValue());
		livro1M.setNome("Harry Potter");
		livro1M.setDisponibilidade(disponibilidade);
		
		Biblioteca livro2M = new Biblioteca();
		livro2M.setId(new Double(Math.random() * 100).intValue());
		livro2M.setNome("Game of Thrones");
		livro2M.setDisponibilidade(disponibilidade);	
		
		List<Biblioteca> livrosMock = Arrays.asList(livro1M, livro2M);
		
		BibliotecaService bibliotecaService = new BibliotecaService(bibliotecaDAO);
		
		Mockito.when(bibliotecaService.buscaLivroPorDisponibilidade(disponibilidade)).thenReturn(livrosMock);
		
		List<Biblioteca> livrosEncontrados = bibliotecaService.buscaLivroPorDisponibilidade(disponibilidade);
		
		System.out.println("deveRetornarLivroPorDisponibilidade: " + livrosEncontrados);
		
		assertNotNull(livrosEncontrados);
		
	}
}
