package br.unifacef.atividade.dto;

import java.math.BigDecimal;

public class Caixa {
	private Integer id;
	
	public BigDecimal taxa;

	public BigDecimal total;
	
	public BigDecimal totalPagar;
	
	public BigDecimal totalDesconto;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public BigDecimal getTaxa() {
		return taxa;
	}

	public void setTaxa(BigDecimal taxa) {
		this.taxa = taxa;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}
	
	public void setTotalPagar() {
		this.totalPagar = (this.total.add(this.taxa)).subtract(this.totalDesconto);
	}
	
	public void getTotalPagar() {
		this.totalPagar = (this.total.add(this.taxa)).subtract(this.totalDesconto);
	}
	
	public void setDesconto(BigDecimal totalDisconto) {
		this.totalDesconto = totalDisconto;
	}
	
	public BigDecimal getDesconto() {
		return totalDesconto;
	}
	
	

	@Override
	public String toString() {
		return "Caixa [id=" + id + ", taxa=" + taxa + ", total=" + total + ",Desconto="+totalDesconto+" ,total a Pagar ="+ totalPagar+"]";
	}
}

