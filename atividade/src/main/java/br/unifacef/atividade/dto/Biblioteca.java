package br.unifacef.atividade.dto;

public class Biblioteca {
	
	private Integer id;
	private String nome;
	private String disponibilidade;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}	
	public String getDisponibilidade() {
		return disponibilidade;
	}
	public void setDisponibilidade(String disponibilidade) {
		this.disponibilidade = disponibilidade;
	}
	
	@Override
	public String toString() {		
		return "Livro [Id=" + id + ", Nome=" + nome + ", Disponibilidade=" + disponibilidade + "]";
	}
	
}
