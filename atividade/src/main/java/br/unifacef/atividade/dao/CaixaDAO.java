package br.unifacef.atividade.dao;

import br.unifacef.atividade.dto.Caixa;


public class CaixaDAO {
	
	public Caixa realizarVenda(final Caixa caixa) {
		
		if(caixa != null && caixa.getId() == null) {
			caixa.setId(new Double(Math.random() * 100).intValue());
		}
		
		return caixa;
	}

}
