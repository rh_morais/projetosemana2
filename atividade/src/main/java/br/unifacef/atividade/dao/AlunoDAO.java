package br.unifacef.atividade.dao;

import java.util.Arrays;
import java.util.List;

import br.unifacef.atividade.dto.Aluno;

public class AlunoDAO {

	public Aluno salvarAluno(final Aluno aluno) {
		
		if(aluno != null && aluno.getId() == null)
			aluno.setId(new Double(Math.random() * 100).intValue());
		
		return aluno;
	}
	
	public List<Aluno> buscarAlunoPorNome(String nome){
		
		return Arrays.asList(
			new Aluno(1, "rafael", (byte)22),
			new Aluno(2, "rafael henrique", (byte)21),
			new Aluno(3, "rafael morais", (byte)23)
		);		
	}	
	
	public Aluno buscarAlunoPorId(Integer id) {
		
		return new Aluno(1, "rafael", (byte)22);		
	}
}
