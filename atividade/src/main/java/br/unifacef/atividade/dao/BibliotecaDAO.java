package br.unifacef.atividade.dao;

import java.util.Arrays;
import java.util.List;

import br.unifacef.atividade.dto.Biblioteca;

public class BibliotecaDAO {
	
	public Biblioteca salvarLivro(Biblioteca biblioteca) {
		if(biblioteca != null && biblioteca.getId() == null) {
			biblioteca.setId(new Double(Math.random() * 100).intValue());
		}
		
		return biblioteca;
	}
	
	public Biblioteca buscaLivroPorId(Integer id) {
		Biblioteca l1 = new Biblioteca();
		l1.setId(id);
		l1.setNome("Harry Potter");
		l1.setDisponibilidade("Disponivel");
		
		return l1;
	}
	
	public List<Biblioteca> buscaLivroPorDisponibilidade(String disponibilidade) {
		Biblioteca l1 = new Biblioteca();
		l1.setId(new Double(Math.random() * 100).intValue());
		l1.setNome("Harry Potter");
		l1.setDisponibilidade(disponibilidade);
		
		Biblioteca l2 = new Biblioteca();
		l2.setId(new Double(Math.random() * 100).intValue());
		l2.setNome("Game of Thrones");
		l2.setDisponibilidade(disponibilidade);		
		
		return Arrays.asList(l1, l2);
	}

}
