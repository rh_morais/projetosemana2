package br.unifacef.atividade.dao;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import br.unifacef.atividade.dto.Professor;

public class ProfessorDAO {

	private List<Professor> lista = new ArrayList<Professor>();
	
	public ProfessorDAO() {
		this.lista.add(new Professor(1, "Iago Colbacho Bettarello", new Date(), "bettarello.iago@gmail.com", "14403430", 
				"Av. Dr. Ismael Alonso y Alonso", "2400", 
				"São José", "Franca", "SP"));
		this.lista.add(new Professor(2, "Juan Leonardo da Silva", new Date(), "juanleonardodasilva@megasurgical.com.br", "32603105", 
				"Rua Presidente Vargas", "605", 
				"Santa Inês", "Betim", "MG"));
		this.lista.add(new Professor(3, "Giovana Luzia da Mota", new Date(), "giovanaluziadamota@signa.net.br", "68901340", 
				"Rua Roberto Ferreira da Silva", "117", 
				"Santa Rita", "Macapá", "AP"));
		this.lista.add(new Professor(4, "Tatiane Patrícia Lima", new Date(), "tatianepatricialima@consultorialk.com.br", "97030842", 
				"Rua Vereador José Manoel da Silveira", "192", 
				"Pinheiro Machado", "Santa Maria", "RS"));
	}

	public Professor salvarProfessor(final Professor professor) {
		
		if(professor != null && professor.getId() == null) {
			professor.setId(new Double(Math.random() * 100).intValue());
		}
		
		return professor;
	}
	
	public List<Professor> buscarProfessorPorNome(String nome){
		
		List<Professor> resultado = new ArrayList<Professor>();
		
		for (Professor p : this.lista) {
			if (p.getNome().contains(nome)) {
				resultado.add(p);
			}
		}
		
		return resultado;
	}	
	
	public Professor buscarProfessorPorId(Integer id) {
		
		Professor resultado = null;
		
		for (Professor p : this.lista) {
			if (p.getId().equals(id)) {
				resultado = p;
			}
		}
		
		return resultado;
	}
}
