package br.unifacef.atividade.dao;

import java.util.Arrays;
import java.util.List;

import br.unifacef.atividade.dto.Disciplina;

public class DisciplinaDAO {
	
	public Disciplina salvarDisciplina(final Disciplina disciplina) {
		
		if (disciplina != null && disciplina.getId() == null) {
			disciplina.setId(new Double(Math.random() * 100).intValue());
		}
		
		return disciplina;
	}
	
	public Disciplina buscaDisciplinaPorId(Integer id) {
		Disciplina d1 = new Disciplina();
		d1.setId(id);
		d1.setNome("Português");
		d1.setPeriodo("Noturno");
		
		return d1;
	}
	
	public List<Disciplina> buscaDisciplinasPorPeriodo(String periodo) {
		Disciplina d1 = new Disciplina();
		d1.setId(new Double(Math.random() * 100).intValue());
		d1.setNome("Português");
		d1.setPeriodo(periodo);
		
		Disciplina d2 = new Disciplina();
		d2.setId(new Double(Math.random() * 100).intValue());
		d2.setNome("Matemática");
		d2.setPeriodo(periodo);
		
		return Arrays.asList(d1, d2);
	}
}
