package br.unifacef.atividade.service;

import java.util.List;

import br.unifacef.atividade.dao.AlunoDAO;
import br.unifacef.atividade.dto.Aluno;

public class AlunoService {

	private AlunoDAO alunoDAO;
	
	public AlunoService(AlunoDAO alunoDAO) {
		this.alunoDAO = alunoDAO;
	}
	
	public Aluno salvarAluno(Aluno aluno) {
		return this.alunoDAO.salvarAluno(aluno);
	}
	
	public List<Aluno> buscarAlunoPorNome(String nome){
		return this.alunoDAO.buscarAlunoPorNome(nome);
	}	
	
	public Aluno buscarAlunoPorId(Integer id){
		return this.alunoDAO.buscarAlunoPorId(id);
	}	
}
