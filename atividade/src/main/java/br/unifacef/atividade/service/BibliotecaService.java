package br.unifacef.atividade.service;

import java.util.List;

import br.unifacef.atividade.dao.BibliotecaDAO;
import br.unifacef.atividade.dto.Biblioteca;

public class BibliotecaService {
	
	private BibliotecaDAO bibliotecaDAO;

    public BibliotecaService(BibliotecaDAO bibliotecaDAO) {
    	this.bibliotecaDAO = bibliotecaDAO;
    }
	
    public Biblioteca salvarLivro(final Biblioteca livro) {
		return this.bibliotecaDAO.salvarLivro(livro);
	}
    
    public Biblioteca buscaLivroPorId(Integer id) {
		return this.bibliotecaDAO.buscaLivroPorId(id);
	}
    
    public List<Biblioteca> buscaLivroPorDisponibilidade(String disponibilidade) {
		return this.bibliotecaDAO.buscaLivroPorDisponibilidade(disponibilidade);
	}
	
}
