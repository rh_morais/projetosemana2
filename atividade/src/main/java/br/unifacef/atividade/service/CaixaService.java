package br.unifacef.atividade.service;

import br.unifacef.atividade.dao.CaixaDAO;
import br.unifacef.atividade.dto.Caixa;

public class CaixaService {
	private CaixaDAO caixaDAO;
	
	public CaixaService(CaixaDAO caixaDAO) {
		this.caixaDAO = caixaDAO;
	}

	public Caixa realizarVenda(Caixa venda) {
		
		return this.caixaDAO.realizarVenda(venda);
	}

}
