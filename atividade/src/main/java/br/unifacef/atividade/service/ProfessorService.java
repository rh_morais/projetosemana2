package br.unifacef.atividade.service;

import java.util.List;

import br.unifacef.atividade.dao.ProfessorDAO;
import br.unifacef.atividade.dto.Professor;

public class ProfessorService {

	private ProfessorDAO dao;
	
	public ProfessorService(ProfessorDAO profDao) {
		this.dao = profDao;
	}
	
	public Professor salvarProfessor(Professor professor) {
		return this.dao.salvarProfessor(professor);
	}
	
	public List<Professor> buscarProfessorPorNome(String nome){
		return this.dao.buscarProfessorPorNome(nome);
	}	
	
	public Professor buscarProfessorPorId(Integer id){
		return this.dao.buscarProfessorPorId(id);
	}
}
