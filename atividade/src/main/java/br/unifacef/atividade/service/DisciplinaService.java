package br.unifacef.atividade.service;

import java.util.List;

import br.unifacef.atividade.dao.DisciplinaDAO;
import br.unifacef.atividade.dto.Disciplina;

public class DisciplinaService {
	private DisciplinaDAO disciplinaDAO;
	
	public DisciplinaService(DisciplinaDAO disciplinaDAO) {
		this.disciplinaDAO = disciplinaDAO;
	}
	
	/**
	 * Método responsável por validar e salvar uma disciplina
	 * @param disciplina
	 * @return objeto disciplina
	 */
	public Disciplina salvarDisciplina(final Disciplina disciplina) {
		return this.disciplinaDAO.salvarDisciplina(disciplina);
	}
	
	/**
	 * Método responsável por realizar a busca de disciplina por ID
	 * @param id
	 * @return objeto disciplina
	 */
	public Disciplina buscaDisciplinaPorId(Integer id) {
		return this.disciplinaDAO.buscaDisciplinaPorId(id);
	}
	
	/**
	 * Método responsável por buscar disciplinas por período 
	 * (Manhã, tarde e noite)
	 * @param periodo
	 * @return lista de disciplinas encontradas
	 */
	public List<Disciplina> buscaDisciplinasPorPeriodo(String periodo) {
		return this.disciplinaDAO.buscaDisciplinasPorPeriodo(periodo);
	}
	
	
}
