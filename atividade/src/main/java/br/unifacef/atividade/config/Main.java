package br.unifacef.atividade.config;

import java.math.BigDecimal;
import java.util.Date;

import br.unifacef.atividade.dao.BibliotecaDAO;
import br.unifacef.atividade.dao.CaixaDAO;
import br.unifacef.atividade.dao.DisciplinaDAO;
import br.unifacef.atividade.dto.Biblioteca;
import br.unifacef.atividade.dao.ProfessorDAO;
import br.unifacef.atividade.dto.Caixa;
import br.unifacef.atividade.dto.Disciplina;
import br.unifacef.atividade.service.BibliotecaService;
import br.unifacef.atividade.dto.Professor;
import br.unifacef.atividade.service.CaixaService;
import br.unifacef.atividade.service.DisciplinaService;
import br.unifacef.atividade.service.ProfessorService;

public class Main {
	
	public static void main (String...strings) {
	
		System.out.println("app buildando...");

		System.out.println("Salvando um novo caixa...");
		
		Caixa novoCaixa = new Caixa();
		
		novoCaixa.setTaxa(new BigDecimal(500));
		novoCaixa.setTotal(new BigDecimal(1000));
		novoCaixa.setDesconto(new BigDecimal(300));
		novoCaixa.setTotalPagar();
		
		CaixaService caixaService = new CaixaService(new CaixaDAO());
		Caixa caixaRegistrada = caixaService.realizarVenda(novoCaixa);
		
		System.out.println(caixaRegistrada);
		
		System.out.println("Salvando uma nova disciplina...");
		
		Disciplina novaDisciplina = new Disciplina();
		novaDisciplina.setNome("Português");
		novaDisciplina.setPeriodo("Noturno");
		
		DisciplinaService disciplinaService = new DisciplinaService(new DisciplinaDAO());
		Disciplina disciplinaRegistrada = disciplinaService.salvarDisciplina(novaDisciplina);
		
		System.out.println(disciplinaRegistrada);
		
		System.out.println("Salvando um novo livro...");
		
		Biblioteca novoLivro = new Biblioteca();
		novoLivro.setNome("Harry Potter");
		novoLivro.setDisponibilidade("Disponivel");	
		
		BibliotecaService bibliotecaService = new BibliotecaService(new BibliotecaDAO());
		Biblioteca livroRegistrado = bibliotecaService.salvarLivro(novoLivro);
		
		System.out.println(livroRegistrado);
		
		System.out.println("Salvando um novo Professor...");
		
		Professor professor = new Professor(1, "Iago Colbacho Bettarello", new Date(), "bettarello.iago@gmail.com", 
				"14620000", "R 1", "1234", "Centro", "Orlândia", "SP");
		ProfessorService ps = new ProfessorService(new ProfessorDAO());
		Professor novoProf = ps.salvarProfessor(professor);
		
		System.out.println(novoProf.toString());
	}
}

